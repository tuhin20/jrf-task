<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ReligionServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot()
    {
        $religions = [
            'Christianity',
            'Islam',
            'Islam',
            'Islam',
            'Islam',
            // Add more religions as needed
        ];

        $this->app->bind('religions', function () use ($religions) {
            return $religions;
        });
        $gender = [
            'Male',
            'Female',
            'Female',
            'Female',
            // Add more values as needed
        ];

        $this->app->bind('gender', function () use ($gender) {
            return $gender;
        });

    }
}
