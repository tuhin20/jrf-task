<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * created product page the form for creating a new resource.
     */
    public function create()
    {
        $products = Product::orderBy('id', 'desc')->get();
        return view('product.index',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function storeOrUpdate(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'description' => 'nullable|string',
        ]);

        // Check if the request contains a product ID for updating
        $productId = $request->input('id');
// dd($productId);
        // Update existing product or create a new one
        $product = Product::updateOrCreate(
            ['id' => $productId], // Search condition
            $validatedData // Data to update or create
        );

        // Optionally, you can return a response or redirect
        return response()->json(['message' => 'Product created or updated successfully', 'product' => $product]);
    }
    
    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        // dd($id);
        $products = Product::findOrFail($id);
        return response()->json(['status' => 'success', 'data' => $products]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        try {
            $product = Product::findOrFail($id);

            $product->name = $request->name;
            $product->price = $request->price;
            $product->description = $request->description;

            $product->save();

            return response()->json($product);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Failed to update product.'], 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
         $product = Product::find($id);
        if (!$product) {
            return response()->json(['message' => 'Product not found.'], 404);
        }
        $product->delete();
        return response()->json(['message' => 'Product deleted successfully.']);
    }
}
