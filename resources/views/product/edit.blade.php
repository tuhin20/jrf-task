@extends('layout.master')
@section('content')
    <div class="container">
        <h1>Edit Product</h1>
        <div class="card-body py-4 card" id="wizard-controller">
            <form id="editProductForm">
                @csrf
                <div class="mb-3 row">
                    <label for="productName" class="col-sm-2 col-form-label">Product Name</label>
                    <div class="col-sm-10">
                        <input type="hidden" value="{{ $products->id }}" name="id" class="form-control" id="productId"
                            placeholder="Enter product name">
                        <input type="text" value="{{ $products->name }}" name="name" class="form-control"
                            id="productName" placeholder="Enter product name">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="productPrice" class="col-sm-2 col-form-label">Product Price</label>
                    <div class="col-sm-10">
                        <input name="price" value="{{ $products->price }}" type="number" class="form-control"
                            id="productPrice" placeholder="Enter product price">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="productDescription" class="col-sm-2 col-form-label">Product Description</label>
                    <div class="col-sm-10">
                        <input type="text" value="{{ $products->description }}" name="description" class="form-control"
                            id="productDescription" placeholder="Enter product description">
                    </div>
                </div>

                <div class="mb-3 row">
                    <div class="col-sm-10 offset-sm-2">
                        <button type="submit" class="btn btn-primary px-5">Update</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection
@section('js')
    <!-- Your custom JavaScript code -->
    <script>
        $(document).ready(function() {
            // Event delegation for edit button
            // Submit the edit form
            $('#editProductForm').submit(function(event) {
                // alert("hhjhj");
                event.preventDefault();
                var formData = $(this).serialize();
                var productId = $('#productId').val();
                // alert(productId);
                $.ajax({
                    type: 'post',
                    url: "{{ url('product/update') }}/" + productId,
                    data: formData,
                    success: function(response) {
                        alert('Product updated successfully.');

                        // Redirect to the list page after update
                        window.location.href = "{{ url('product/add') }}";

                    },
                    error: function(xhr, status, error) {
                        console.error(xhr.responseText);
                        alert('Failed to update product.');
                    }
                });
            });
        });
    </script>
@endsection
