@extends('layout.master')
@section('content')
    <div class="container">
        <h1>Product Add</h1>
        <div class="row">
            <div class="col-md-4 card mr-2">
                <form id="productAddForm" class="p-5">
                    @csrf
                    <!-- Hidden input field for product_id -->
                    <input type="hidden" name="id" id="product_id" value="">
                    <div class="mb-4 row">
                        <label for="productName" class="">Product Name</label>
                        <input type="text" name="name" class="form-control" id="name"
                            placeholder="Enter product name">
                    </div>
                    <div class="mb-4 row">
                        <label for="productPrice" class="">Product Price</label>

                        <input name="price" type="number" class="form-control" id="price"
                            placeholder="Enter product price">
                    </div>
                    <div class="mb-4 row">
                        <label for="productDescription" class="">Product Description</label>
                        <input type="text" name="description" class="form-control" id="description"
                            placeholder="Enter product description">

                    </div>
                    <div class="mb-4 row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary px-5 float-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
            {{-- <div class="col-md-1"></div> --}}
            <div class="col-md-7 card">
                <div class="___table p-2">
                    <div class="table-responsive scrollbar">
                        <table class="table table-bordered">
                            <thead class="table-active">
                                <tr>
                                    <th scope="col">Product Name</th>
                                    <th scope="col">Price</th>
                                    <th scope="col">Description</th>
                                    <th class="text-end" scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody id="productList">
                                @foreach ($products as $row)
                                    <tr>
                                        <td>{{ $row->name }}</td>
                                        <td>{{ $row->price }}</td>
                                        <td>{{ $row->description }}</td>
                                        <td class="text-end">
                                            <button class="btn btn-primary edit-product"
                                                data-product-id="{{ $row->id }}">Edit</button>
                                            <button class="btn btn-danger delete-product"
                                                data-product-id="{{ $row->id }}">Delete</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            // Event listener for edit-product button click
            $('.edit-product').click(function() {
                var productId = $(this).data('product-id');
                // alert(productId);
                // Fetch product data using AJAX
                $.ajax({
                    type: 'get',
                    url: "{{ url('product/edit') }}" + '/' + productId,
                    // data: formData,
                    success: function(response) {
                        // Handle success response
                        // console.log(response.data);
                        $('#product_id').val(response.data.id);
                        $('#name').val(response.data.name);
                        $('#price').val(response.data.price);
                        $('#description').val(response.data.description);
                        // alert(response.message);
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to save product.');
                    }
                });
                // Fill the form fields with product data

            });

            // Listen for the form submission
            $('#productAddForm').submit(function(event) {
                event.preventDefault();
                // Serialize form data
                var formData = $(this).serialize();
                // Perform AJAX call
                $.ajax({
                    type: 'POST',
                    url: "{{ url('product/storeOrUpdate') }}",
                    data: formData,
                    success: function(response) {
                        // Handle success response
                        alert(response.message);
                        // Redirect to the product add page
                        window.location.href = '/product/add';
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to save product.');
                    }
                });
            });

            // Event listener for delete-product button click
            $('.delete-product').click(function(event) {
                event.preventDefault();
                var productId = $(this).data('product-id');
                // Send AJAX request to delete the product
                $.ajax({
                    type: 'DELETE',
                    url: "{{ url('product/delete') }}/" + productId,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        // Handle success response
                        alert('Product deleted successfully.');
                        // Reload the page to update the product list
                        location.reload();
                    },
                    error: function(xhr, status, error) {
                        // Handle error response
                        console.error(xhr.responseText);
                        alert('Failed to delete product.');
                    }
                });
            });
        });
    </script>
@endsection
