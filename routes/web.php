<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use App\Models\Product;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [ProductController::class, 'create'])->name('product.add');
Route::prefix('product')->group(function () {
    Route::get('/add', [ProductController::class, 'create'])->name('product.add');
    Route::post('/storeOrUpdate', [ProductController::class, 'storeOrUpdate']);
    Route::delete('/delete/{id}', [ProductController::class, 'destroy'])->name('product.destroy');
    Route::get('/edit/{id}', [ProductController::class, 'edit'])->name('product.edit');
    Route::post('/update/{id}', [ProductController::class, 'update'])->name('product.update');

 });
